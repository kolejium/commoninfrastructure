﻿using System;

namespace CommonInfrastructure.Common.Core.Connections
{
    /// <summary>
    /// Описывает основные методы и свойства подключения
    /// </summary>
    public interface IConnection
    {
        /// <summary>
        /// Индикатор состояние подключения (есть соединенение)
        /// </summary>
        bool IsConnected { get; }

        /// <summary>
        /// Таймаут подключения
        /// </summary>
        int Timeout { get; }

        /// <summary>
        /// События открытия подключения
        /// </summary>
        event Action Opened;

        /// <summary>
        /// Событие закрытия подключения
        /// </summary>
        event Action Closed;

        /// <summary>
        /// Открыть подключение
        /// </summary>
        void Open();

        /// <summary>
        /// Закрыть подключение
        /// </summary>
        void Close();
    }
}