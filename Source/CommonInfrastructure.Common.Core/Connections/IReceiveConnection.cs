﻿using System;

namespace CommonInfrastructure.Common.Core.Connections
{
    /// <summary>
    /// Описывает основные свойства и методы соединения для получения данных,
    /// наследуется от <see cref="IConnection"/>
    /// </summary>
    public interface IReceiveConnection : IConnection
    {
        /// <summary>
        /// Получить данные
        /// </summary>
        /// <returns>данные</returns>
        byte[] Receive();

        /// <summary>
        /// Таймаут получения данных
        /// </summary>
        int ReceiveTimeout { get; }

        /// <summary>
        /// Индикатор состояния наличия данных для чтения
        /// </summary>
        bool IsDataAvailable { get; }

        /// <summary>
        /// Событие наличия данных для чтения
        /// </summary>
        event Action DataAvailable;
    }
}