﻿namespace CommonInfrastructure.Common.Core.Connections
{
    /// <summary>
    /// Описывает основные свойства и методы соединения для отправки данных,
    /// наследуется от <see cref="IConnection"/>
    /// </summary>
    public interface ISendConnection : IConnection
    {
        /// <summary>
        /// Отправить данные
        /// </summary>
        /// <param name="data">данные</param>
        void Send(byte[] data);

        /// <summary>
        /// Таймаут отправки
        /// </summary>
        int SendTimeout { get; }
    }
}