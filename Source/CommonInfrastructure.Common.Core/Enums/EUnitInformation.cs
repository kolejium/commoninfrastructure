﻿namespace CommonInfrastructure.Common.Core.Enums
{
    /// <summary>
    /// Перечисление единиц информации
    /// </summary>
    public enum EUnitInformation
    {
        /// <summary>
        /// Бит
        /// </summary>
        Bit,
        /// <summary>
        /// Байт
        /// </summary>
        Byte
    }
}