﻿using System;

namespace CommonInfrastructure.Common.Core.Logger
{
    /// <summary>
    /// Описывает основные методы и события логгера
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Ошибка логгера
        /// </summary>
        event Action<Exception> Error;

        /// <summary>
        /// Записать сообщение
        /// </summary>
        /// <param name="message">сообщение</param>
        void Write(string message);

        /// <summary>
        /// Записать сообщение
        /// </summary>
        /// <param name="message">сообщение</param>
        /// <param name="format">формат сообщения</param>
        void Write(string message, IFormatProvider format);
    }
}