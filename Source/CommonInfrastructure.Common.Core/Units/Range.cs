﻿namespace CommonInfrastructure.Common.Core.Units
{
    /// <summary>
    /// Структура описания диапазона
    /// </summary>
    /// <typeparam name="T">единица измерения</typeparam>
    /// <typeparam name="U">тип для хранения значения (начального и конечного)</typeparam>
    public struct Range<T, U>
    {
        /// <summary>
        /// Единица измерения
        /// </summary>
        public T Unit { get; set; }
        
        /// <summary>
        /// Начало диапазона
        /// </summary>
        public U Start { get; set; }

        /// <summary>
        /// Конец диапазона
        /// </summary>
        public U End { get; set; }
    }
}