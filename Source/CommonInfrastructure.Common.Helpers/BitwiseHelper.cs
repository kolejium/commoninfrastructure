﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CommonInfrastructure.Common.Core.Enums;

namespace CommonInfrastructure.Common.Helpers
{
    /// <summary>
    /// Помощник для работы с поразрядовыми операциями
    /// </summary>
    public static class BitwiseHelper
    {
        /// <summary>
        /// Сдвинуть значение на количество бит
        /// </summary>
        /// <param name="value">значение</param>
        /// <param name="countBits">количество бит сдвига. <remarks>Положительное число сдвиг влево, отрицательное вправо</remarks>
        /// </param>
        /// <returns></returns>
        public static ulong Shift(ulong value, int countBits)
        {
            return countBits != 0 ? (countBits > 0 ? value << countBits : value >> countBits) : value;
        }

        /// <inheritdoc cref="Shift(ulong,int)"/>
        public static uint Shift(uint value, int countBits) => (uint) Shift((ulong) value, countBits);

        internal static byte[] SubarrayBytes(byte[] array, int start, int count)
        {
            if (start > array.Length || start < 0)
                throw new ArgumentOutOfRangeException(nameof(start));

            if (count > array.Length || start + count > array.Length)
                throw new ArgumentOutOfRangeException(nameof(count));

            var buffer = new byte[count];
            var index = 0;

            for (int i = start; i < start+count; i++)
            {
                buffer[index] = array[i];
                index++;
            }

            return buffer;
        }

        internal static byte[] SubarrayBits(byte[] array, int start, int count)
        {
            if (start > array.Length * 8 || start < 0)
                throw new ArgumentOutOfRangeException(nameof(start));

            if (start + count > array.Length * 8)
                throw new ArgumentOutOfRangeException(nameof(count));

            var startIndex = start / 8;
            var endIndex = (count + start) % 8 == 0 ? (count + start) / 8 : (count + start) / 8 + 1;


            var buffer = new byte[endIndex - startIndex];
            var bufferIndex = 0;

            for (int i = startIndex; i < endIndex; i++)
            {
                buffer[bufferIndex] = array[i];
                bufferIndex++;
            }

            buffer[0] = (byte) (buffer[0] >> start);
            buffer[buffer.Length - 1] &= (byte)((2 << (count - (count / 8) * 8)) - 1);

            return buffer;
        }

        public static byte[] Subarray(byte[] array, int start, int count, EUnitInformation unit = EUnitInformation.Byte)
        {
            if (array == null)
                throw new ArgumentNullException(nameof(array));

            if (array.Length == 0)
                throw new ArgumentException("Массив пуст", nameof(array));

            switch (unit)
            {
                case EUnitInformation.Bit:
                    return SubarrayBits(array, start, count);
                case EUnitInformation.Byte:
                default:
                    return SubarrayBytes(array, start, count);
            }
        }
    }
}