﻿using System.ComponentModel;
using CommonInfrastructure.UI.Common.Views;

namespace CommonInfrastructure.UI.Common.ViewModels
{
    /// <summary>
    /// Основные требования которым должна соответсвовать модель представления данных
    /// </summary>
    public interface IViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Модель представления окна к которой относиться данная модель представления данных
        /// </summary>
        IView View { get; set; }
    }
}