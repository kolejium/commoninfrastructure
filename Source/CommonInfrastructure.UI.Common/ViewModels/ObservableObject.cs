﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using CommonInfrastructure.UI.Common.Annotations;

namespace CommonInfrastructure.UI.Common.ViewModels
{
    /// <summary>
    /// Объект, который может оповещать другие объекты об изменении своего состояния
    /// </summary>
    public class ObservableObject : INotifyPropertyChanged
    {
        /// <summary>
        /// Событие изменение значений свойства
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Оповестить об изменении свойства
        /// </summary>
        /// <param name="propertyName">наименование свойства</param>
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}