﻿using CommonInfrastructure.UI.Common.Views;

namespace CommonInfrastructure.UI.Common.ViewModels
{
    /// <summary>
    /// Стандартная реализация класса модели-представления в парадигме MVVM
    /// </summary>
    public class ViewModel : ObservableObject, IViewModel
    {
        private IView _view;

        /// <inheritdoc cref="IViewModel.View"/>
        public IView View
        {
            get => _view;
            set
            {
                if (value == _view)
                    return;

                _view = value;
                OnPropertyChanged(nameof(View));
            }
        }
    }
}