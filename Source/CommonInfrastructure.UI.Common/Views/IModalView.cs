﻿namespace CommonInfrastructure.UI.Common.Views
{
    /// <summary>
    /// Интерфейс модального представления
    /// </summary>
    public interface IModalView : IView
    {
        /// <summary>
        /// Показать представление
        /// </summary>
        /// <returns>результат взаимодействия</returns>
        ModalViewResult ShowModal();
    }

    /// <inheritdoc cref="IModalView"/>
    public interface IModalView<T> : IModalView
    {
        /// <inheritdoc cref="IModalView"/>
        new ModalViewResult<T> ShowModal();
    }
}