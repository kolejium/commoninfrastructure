﻿using System;
using System.Collections.Generic;

namespace CommonInfrastructure.UI.Common.Views
{
    /// <summary>
    /// Представляет основной функционал модели представления окна в парадигме MVVM
    /// </summary>
    public interface IView
    {
        /// <summary>
        /// Наименование представления
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Показать представление
        /// </summary>
        void Show();

        /// <summary>
        /// Скрыть представление
        /// </summary>
        void Hide();

        /// <summary>
        /// Закрыть представление
        /// </summary>
        void Close();

        /// <summary>
        /// Дочерние представления
        /// </summary>
        IEnumerable<IView> ChildViews { get; }

        /// <summary>
        /// Событие закрытия представления
        /// Параметры: представление
        /// </summary>
        event Action<IView> Closed;

        /// <summary>
        /// Событие отображения представления
        /// Параметры: представление
        /// </summary>
        event Action<IView> Shown;
    }
}