﻿namespace CommonInfrastructure.UI.Common.Views
{
    /// <summary>
    /// Результат взаимодействия с диалоговым представлением
    /// </summary>
    public class ModalViewResult
    {
        /// <summary>
        /// Создать результат взаимодействия пользователя с диалоговым представлением
        /// </summary>
        /// <param name="isAccepted">подтверждено ли диалоговое представление (okay/cancel)</param>
        public ModalViewResult(bool isAccepted)
        {
            IsAccepted = isAccepted;
        }

        /// <summary>
        /// Индикатор подтверждения пользователем диалогового представления
        /// <remarks>true - подтвержден, false - отклонен</remarks>
        /// </summary>
        public bool IsAccepted { get; }
    }

    /// <summary>
    /// Результат взаимодействия пользователя с диалоговым представлением
    /// </summary>
    /// <typeparam name="T">тип возвращаемых данных</typeparam>
    public class ModalViewResult<T> : ModalViewResult
    {
        /// <summary>
        /// <inheritdoc cref="ModalViewResult"/>
        /// </summary>
        /// <param name="isAccepted">подтверждено ли диалоговое представление</param>
        /// <param name="result">данные переданные пользователем</param>
        public ModalViewResult(bool isAccepted, T result) : base(isAccepted) => Result = result;

        /// <summary>
        /// Данные которые подтвердил пользователь
        /// </summary>
        public T Result { get; }
    }
}