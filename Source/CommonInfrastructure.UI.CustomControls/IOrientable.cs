﻿using System.Collections;
using System.Collections.Generic;

namespace CommonInfrastructure.UI.CustomControls
{
    /// <summary>
    /// Характеризует ориентируемый элемент управления
    /// </summary>
    /// <typeparam name="TOrientation">тип определяющий ориентацию на используемой платформе</typeparam>
    public interface IOrientable<TOrientation>
    {
        /// <summary>
        /// Текущая ориентация
        /// </summary>
        TOrientation Orientation { get; set; }

        /// <summary>
        /// Сменить ориентацию
        /// </summary>
        void ChangeOrientation();

        /// <summary>
        /// Сменить ориентация
        /// </summary>
        /// <param name="orientation">ориентация</param>
        void ChangeOrientation(TOrientation orientation);
    }
}