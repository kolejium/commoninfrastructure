﻿using System;
using System.Windows.Input;

namespace CommonInfrastructure.UI.WPF.Commands
{
    /// <summary>
    /// Команда инкапсулирующая ссылку на метод 
    /// </summary>
    public class ActionCommand : ICommand
    {
        private readonly Action _commandCallback;
        private readonly Func<bool> _canExecuteCommandCallback;

        /// <summary>
        /// Событие изменение возможности выполнения команды
        /// </summary>
        public event EventHandler CanExecuteChanged;

        /// <summary>
        /// Создать экземпляр команды инкапсулирующий ссылку на метод
        /// </summary>
        /// <param name="commandCallback">ссылка на метод</param>
        /// <param name="canExecuteCommandCallback">ссылка на метод возможности выполнения команды</param>
        public ActionCommand(Action commandCallback, Func<bool> canExecuteCommandCallback = null)
        {
            _commandCallback = commandCallback;
            _canExecuteCommandCallback = canExecuteCommandCallback;
        }

        /// <summary>
        /// Возможно ли выполнение команды
        /// </summary>
        /// <param name="parameter">параметер</param>
        /// <returns>индикатор возможности выполнения команды</returns>
        public bool CanExecute(object parameter)
        {
            return _canExecuteCommandCallback?.Invoke() ?? true;
        }

        /// <summary>
        /// Выполнить команду
        /// </summary>
        /// <param name="parameter">параметер</param>
        public void Execute(object parameter)
        {
            if (!CanExecute(parameter))
                return;

            _commandCallback?.Invoke();
        }
    }
}
