﻿using CommonInfrastructure.UI.Common.Views;

namespace CommonInfrastructure.UI.WPF.Views
{
    /// <summary>
    /// Абстрактный класс модели представления модального окна реализующий интерфейс <see cref="IModalView"/>.
    /// Предоставляя его базовую реализацию.
    /// </summary>
    public abstract class ModalView : View, IModalView
    {
        /// <inheritdoc cref="IModalView.ShowModal"/>
        public virtual ModalViewResult ShowModal()
        {
            var shown = Window.ShowDialog();

            return new ModalViewResult(shown.HasValue && shown.Value);
        }
    }

    /// <inheritdoc cref="ModalView" />
    /// <typeparam name="T">тип результата</typeparam>
    public abstract class ModalView<T> : ModalView, IModalView<T>
    {
        /// <inheritdoc cref="ModalView.ShowModal"/>
        public new virtual ModalViewResult<T> ShowModal()
        {
            var shown = Window.ShowDialog();

            return new ModalViewResult<T>(shown.HasValue && shown.Value, GetResult());
        }

        /// <summary>
        /// Получить результат взаимоодействия с модальным окном
        /// </summary>
        /// <returns>результат взаимоодействия с модальным окном</returns>
        public abstract T GetResult();
    }
}
