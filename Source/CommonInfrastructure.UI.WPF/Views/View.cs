﻿using System;
using System.Collections.Generic;
using System.Windows;
using CommonInfrastructure.UI.Common.Views;

namespace CommonInfrastructure.UI.WPF.Views
{
    /// <summary>
    /// Абстрактный класс модели представления окна реализующий интерфейс <see cref="IView"/>.
    /// Предоставляя его базовую реализацию.
    /// </summary>
    public abstract class View : IView
    {
        /// <summary>
        /// Окно представления
        /// </summary>
        protected abstract Window Window { get; }

        /// <summary>
        /// Внутрений список дочерних представлений
        /// </summary>
        protected List<IView> InternalChildViews { get; }

        /// <inheritdoc cref="IView.Name"/>
        public abstract string Name { get; }

        /// <summary>
        /// Перечень дочерних представлений
        /// </summary>
        public IEnumerable<IView> ChildViews => InternalChildViews;

        /// <inheritdoc cref="IView.Closed"/>
        public event Action<IView> Closed;

        /// <inheritdoc cref="IView.Shown"/>
        public event Action<IView> Shown;

        /// <summary>
        /// Создать экземпляр представления
        /// </summary>
        protected View()
        {
            InternalChildViews = new List<IView>();
        }

        /// <inheritdoc cref="IView.Show"/>
        public virtual void Show()
        {
            if (Window.IsLoaded)
                Window.Visibility = Visibility.Visible;
            else
            {
                Window.ContentRendered += OnWindowReady;

                Window.Show();
            }
        }

        /// <inheritdoc cref="IView.Hide"/>
        public void Hide()
        {
            if (Window.IsLoaded)
                Window.Hide();
            else
                Window.Visibility = Visibility.Hidden;
        }

        /// <inheritdoc cref="IView.Close"/>
        public void Close()
        {
            Window.Close();
        }

        /// <summary>
        /// Обработчик события закрытия дочернего представления
        /// </summary>
        /// <param name="childView">дочернее представление</param>
        protected void OnChildViewClosed(IView childView)
        {
            if (childView == null)
                return;

            childView.Closed -= OnChildViewClosed;
            InternalChildViews.RemoveAll(x => ReferenceEquals(x, childView));
        }

        /// <summary>
        /// Обработчик готовности окна
        /// </summary>
        private void OnWindowReady(object sender, EventArgs eventArgs)
        {
            Window.ContentRendered -= OnWindowReady;
        }

        /// <summary>
        /// Обработчик закрытия окна
        /// </summary>
        protected void OnWindowClosed(object sender, EventArgs eventArgs)
        {
            foreach (var child in ChildViews)
            {
                child.Closed -= OnChildViewClosed;
                child.Close();
            }

            InternalChildViews.Clear();

            Closed?.Invoke(this);
        }
    }
}